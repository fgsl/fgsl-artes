﻿1
00:00:00,400 --> 00:00:01,316
Olá,

2
00:00:01,317 --> 00:00:03,240
meu nome é Jon "maddog" Hall,

3
00:00:03,490 --> 00:00:05,796
e eu sou o presidente da Linux Internacional.

4
00:00:06,046 --> 00:00:08,446
Eu gostaria de convidar todos vocês

5
00:00:08,696 --> 00:00:09,740
para a décima segunda edição

6
00:00:09,990 --> 00:00:11,174
deste evento.

7
00:00:12,396 --> 00:00:14,412
Além de aproveitar a companhia de

8
00:00:14,413 --> 00:00:16,184
outros entusiastas de Software Livre,

9
00:00:16,434 --> 00:00:17,934
incluindo eu,

10
00:00:18,184 --> 00:00:20,561
você poderá aprender sobre os ecossistemas e

11
00:00:20,580 --> 00:00:22,772
cultura do Software Livre.

12
00:00:23,635 --> 00:00:25,173
Você também poderá aprender sobre

13
00:00:25,423 --> 00:00:26,923
tópicos como Tecnologia da Informação Verde,

14
00:00:27,173 --> 00:00:30,632
utilizando soluções em Software Livre,

15
00:00:30,470 --> 00:00:33,567
Governo e Software Público,

16
00:00:33,062 --> 00:00:35,851
novamente, usando soluções em Software Livre,

17
00:00:36,000 --> 00:00:37,900
Produtos e serviços comerciais,

18
00:00:37,950 --> 00:00:38,759
que estão disponíveis

19
00:00:39,009 --> 00:00:40,498
e que são de código aberto.

20
00:00:40,748 --> 00:00:43,085
Projetos e ferramentas para usar,

21
00:00:43,335 --> 00:00:46,439
educação e inclusão digital,

22
00:00:46,689 --> 00:00:48,189
utilizando Softwares Livres como ferramentas

23
00:00:48,439 --> 00:00:51,438
desenvolvimento web e design,

24
00:00:51,688 --> 00:00:55,878
desenvolvimento de redes e telecomunicações,

25
00:00:56,128 --> 00:00:57,959
informação e técnicas de segurança

26
00:00:58,209 --> 00:00:59,850
e finalmente robótica.

27
00:01:00,100 --> 00:01:02,210
Há provavelmente vários outros tópicos

28
00:01:02,230 --> 00:01:03,960
que você pode discutir

29
00:01:04,210 --> 00:01:05,574
com as outras pessoas que estarão lá

30
00:01:05,824 --> 00:01:07,324
Então, por favor, junte se a nós!

31
00:01:07,574 --> 00:01:09,553
neste evento

32
00:01:09,803 --> 00:01:13,800
e... lembre de trazer duas pessoas da Microsoft com você.

33
00:01:14,050 --> 00:01:16,146
Meu nome é Jon "maddog" Hall,

34
00:01:16,396 --> 00:01:18,155
sou presidente da Linux Internacional,

35
00:01:18,405 --> 00:01:20,193
e eu espero ver todos vocês lá.

36
00:01:20,443 --> 00:01:21,943
Obrigado.

